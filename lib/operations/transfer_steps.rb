require 'dry-container'
require 'dry/transaction'
require 'dry/transaction/operation'

module Operations
  PersistenceError = Class.new(StandardError)

  class FindSender
    include Dry::Transaction::Operation

    def call(import_row)
      sender = Account.find_by_account_no(import_row.sender_konto)
      if sender.blank?
        Left("#{import_row.activity_id}: Account #{import_row.sender_konto} not found")
      else
        Right(import_row: import_row, sender: sender)
      end
    end
  end

  class TransferSteps
    extend Dry::Container::Mixin

    namespace 'operations' do |ops|
      ops.register :find_sender do
        FindSender.new
      end
    end
  end
end
