require 'iconv'

class AddDtaRow
  include Dry::Transaction(container: Operations::TransferSteps)
  step :verify_sender
  tee :add_buchung

  def verify_sender(input)
    import_row = input.fetch(:import_row)
    dtaus = input.fetch(:dtaus)
    if dtaus.valid_sender?(import_row.sender_konto, import_row.sender_blz)
      Right(import_row: import_row, dtaus: dtaus)
    else
      Left(["#{import_row.activity_id}: BLZ/Konto not valid, csv file not written"])
    end
  end

  def add_buchung(input)
    import_row = input[:import_row]
    dtaus = input[:dtaus]
    holder = Iconv.iconv('ascii//translit', 'utf-8', import_row.sender_name).to_s.gsub(/[^\w^\s]/, '')
    dtaus.add_buchung(
      import_row.sender_konto,
      import_row.sender_blz,
      holder,
      import_row.amount.abs,
      import_row.import_subject
    )
  end
end
