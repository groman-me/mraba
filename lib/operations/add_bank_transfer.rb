class AddBankTransfer
  include Dry::Transaction(container: Operations::TransferSteps)

  step :find_sender, with: 'operations.find_sender'
  step :build_transfer
  step :validate_transfer
  try :persist, catch: Operations::PersistenceError

  def build_transfer(input)
    transfer = input[:sender].build_transfer(input[:import_row].to_bank_transfer_attributes)
    Right(input.merge(transfer: transfer))
  end

  def validate_transfer(input)
    transfer = input[:transfer]
    if transfer.valid?
      Right(input)
    else
      errors = transfer.errors.full_messages.join('; ')
      Left("#{input[:import_row].activity_id}: BankTransfer validation error(s): #{errors}")
    end
  end

  def persist(input, validation_only: false)
    transfer = input[:transfer]
    transfer.save! unless validation_only
    transfer
  rescue => e
    raise Operations::PersistenceError, e.inspect
  end
end
