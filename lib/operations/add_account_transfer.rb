class AddAccountTransfer
  include Dry::Transaction(container: Operations::TransferSteps)

  step :find_sender, with: 'operations.find_sender'
  step :build_transfer
  step :validate_transfer
  try :persist, catch: Operations::PersistenceError

  def build_transfer(input)
    row = input[:import_row]
    sender = input[:sender]
    if row.depot_activity_id.blank?
      account_transfer = build_credit_account_transfer(row, sender)
    else
      transfer_by_id = credit_account_transfer_by_id(row, sender)
      return Left(format_error(row, transfer_by_id.failure)) if transfer_by_id.failure?
      account_transfer = transfer_by_id.value
    end
    Right(input.merge(account_transfer: account_transfer))
  end

  def validate_transfer(input)
    account_transfer = input[:account_transfer]
    if account_transfer.valid?
      Right(input.merge(account_transfer: account_transfer))
    else
      errors = account_transfer.errors.full_messages.join('; ')
      Left(format_error(input[:import_row], "AccountTransfer validation error(s): #{errors}"))
    end
  end

  def persist(input, validation_only: false)
    account_transfer = input[:account_transfer]
    unless validation_only
      if input.fetch(:import_row).depot_activity_id.blank?
        account_transfer.save!
      else
        account_transfer.complete_transfer!
      end
    end
    account_transfer
  rescue => e
    raise Operations::PersistenceError, e.inspect
  end

  private

  def build_credit_account_transfer(row, sender)
    transfer_attrs = { amount: row.amount, subject: row.import_subject, receiver_multi: row.receiver_konto }
    transfer = sender.credit_account_transfers.build(transfer_attrs)
    transfer.date = row.entry_date
    transfer.skip_mobile_tan = true
    transfer
  end

  def credit_account_transfer_by_id(row, sender)
    account_transfer = sender.credit_account_transfers.find_by_id(row.depot_activity_id)
    if account_transfer.nil?
      Left('AccountTransfer not found')
    elsif account_transfer.state != 'pending'
      Left("AccountTransfer state expected 'pending' but was '#{account_transfer.state}'")
    else
      account_transfer.subject = row.import_subject
      Right(account_transfer)
    end
  end

  def format_error(row, message)
    "#{row.activity_id}: #{message}"
  end
end
