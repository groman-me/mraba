require 'dry/transaction'

class ImportFileRow
  include Dry::Transaction
  step :validate_import_row
  step :import

  def validate_import_row(import_row)
    import_row.valid? ? Right(import_row) : Left(import_row.errors)
  end

  def import(import_row, validation_only:, dtaus:, max_retry_count: 5)
    transfer = add_transfer_with_retries(import_row, validation_only, dtaus, max_retry_count)
    if transfer.nil?
      Left(Array("#{import_row.activity_id}: Transaction type not found"))
    elsif transfer.failure?
      Left(Array(transfer.failure))
    else
      Right(transfer.value)
    end
  end

  private

  def add_transfer_with_retries(import_row, validation_only, dtaus, max_retry_count)
    import_retry_count ||= 1
    add_transfer(import_row, validation_only, dtaus)
  rescue => e
    if (import_retry_count += 1) <= max_retry_count
      retry
    else
      Left("#{import_row.activity_id}: #{e.message}")
    end
  end

  def add_transfer(import_row, validation_only, dtaus)
    operation = operation_type(import_row)
    case operation
    when AddAccountTransfer, AddBankTransfer
      operation
        .with_step_args(persist: [validation_only: validation_only])
        .call(import_row)
    when AddDtaRow
      operation.call(import_row: import_row, dtaus: dtaus)
    end
  end

  def operation_type(row)
    if row.account_transfer?
      AddAccountTransfer.new
    elsif row.bank_transfer?
      AddBankTransfer.new
    elsif row.dta_row?
      AddDtaRow.new
    end
  end

  def invalid_row_message(row)
    "#{row.activity_id}: UMSATZ_KEY #{row['UMSATZ_KEY']} is not allowed" unless %w(10 16).include?row['UMSATZ_KEY']
  end
end
