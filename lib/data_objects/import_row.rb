require_relative 'types'

class ImportRow < Dry::Struct
  constructor_type :schema

  ALLOWED_UMSATZ_KEYS = %w(10 16).freeze
  IMPORT_KEYS = %w(
    ACTIVITY_ID DEPOT_ACTIVITY_ID KONTONUMMER AMOUNT CURRENCY ENTRY_DATE VALUE_DATE UMSATZ_KEY UMSATZ_KEY_EXT
    RECEIVER_BLZ RECEIVER_KONTO RECEIVER_NAME SENDER_BLZ SENDER_KONTO SENDER_NAME DESC1 DESC2 DESC3 DESC4 DESC5
    DESC6 DESC7 DESC8 DESC9 DESC10 DESC11 DESC12 DESC13 DESC14 INT_UMSATZ_KEY
  ).freeze
  ALLOWED_ATTRIBUTES = IMPORT_KEYS.map(&:downcase).map(&:to_sym)

  def self.attribute_type(attr)
    if attr.include?('amount')
      Types::Form::Decimal.optional
    elsif attr.include?('date')
      Types::Form::Date.optional
    else
      Types::String.optional
    end
  end

  ALLOWED_ATTRIBUTES.each do |import_key|
    attribute import_key, attribute_type(import_key.to_s)
  end

  def self.from_row(row)
    valid_keys = row.keys & IMPORT_KEYS
    record = valid_keys.each_with_object({}) do |key, attrs|
      attrs[key.downcase.to_sym] = row[key]
    end
    new(record)
  end

  def valid?
    allowed_umsatz_key?
  end

  def errors
    ["#{activity_id}: UMSATZ_KEY #{umsatz_key} is not allowed"] unless allowed_umsatz_key?
  end

  def import_subject
    1.upto(14).reduce('') do |subject, id|
      desc = send("desc#{id}".to_sym)
      desc.blank? ? subject : subject + desc.to_s
    end
  end

  def sender
    return @sender if defined?(@sender)
    @sender = Account.find_by_account_no(sender_konto)
  end

  def to_bank_transfer_attributes
    {
      amount: amount,
      subject: import_subject,
      rec_holder: receiver_name,
      rec_account_number: receiver_konto,
      rec_bank_code: receiver_blz
    }
  end

  def account_transfer?
    sender_blz == '00000000' && receiver_blz == '00000000'
  end

  def bank_transfer?
    sender_blz == '00000000' && umsatz_key == '10'
  end

  def dta_row?
    receiver_blz == '70022200' && umsatz_key == '16'
  end

  private

  def allowed_umsatz_key?
    ALLOWED_UMSATZ_KEYS.include?(umsatz_key)
  end
end
