# encoding: utf-8
require 'net/sftp'
require 'iconv'
require 'csv'

class CsvExporter
  CONFIG = {
    sftp_server: Rails.env == 'production' ? 'csv.example.com/endpoint/' : '0.0.0.0:2020',
    download_dir: "#{Rails.root}/private/data/download",
    ftp_user: 'some-ftp-user',
    ftp_keys: ['path-to-credentials'],
    ftp_entries_dir: '/data/files/csv',
    upload_dir: "#{Rails.root}/private/data/upload",
    processed_files: '/data/files/batch_processed'
  }.freeze

  def self.sftp_session
    Net::SFTP.start(CONFIG[:sftp_server], CONFIG[:ftp_user], keys: CONFIG[:ftp_keys]) do |sftp|
      yield(sftp)
    end
  end

  # Possible performance optimisations:
  #   parallel/concurrent files download (possible with scp) and file processing
  #
  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def self.transfer_and_import(send_email = true)
    FileUtils.mkdir_p CONFIG[:download_dir]

    sftp_session do |sftp|
      sftp_entries = sftp.dir.entries(CONFIG[:ftp_entries_dir]).map(&:name).sort
      sftp_entries.select { |name| name.end_with?('.csv') }.each do |entry|
        next unless sftp_entries.include?(entry + '.start')

        file_local = File.join(CONFIG[:download_dir], entry)
        file_remote = "#{CONFIG[:ftp_entries_dir]}/#{entry}"

        sftp.download!(file_remote, file_local)
        sftp.remove!(file_remote + '.start')
        importer = new
        result = importer.import(file_local)

        if result == 'Success'
          File.delete(file_local)
          if send_email
            BackendMailer.send_import_feedback('Successful Import', "Import of the file #{entry} done.")
          end
        else
          error_content = ["Import of the file #{entry} failed with errors:", result].join("\n")
          importer.upload_error_file(entry, error_content)
          if send_email
            BackendMailer.send_import_feedback('Import CSV failed', error_content)
          end
          break
        end
      end
    end
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength

  def import(file, validation_only = false)
    begin
      import_result = import_file(file, validation_only)
    rescue => e
      import_result = { errors: [e.to_s], success: ['data lost'] }
    end
    result =
      if import_result[:errors].blank?
        'Success'
      else
        "Imported: #{import_result[:success].join(', ')} Errors: #{import_result[:errors].join('; ')}"
      end

    Rails.logger.info "CsvExporter#import time: #{Time.now.to_formatted_s(:db)} Imported #{file}: #{result}"

    result
  end

  def upload_error_file(entry, result)
    FileUtils.mkdir_p CONFIG[:upload_dir]
    error_file = File.join(CONFIG[:upload_dir], entry)
    File.write(error_file, result)
    self.class.sftp_session do |sftp|
      sftp.upload!(error_file, "#{CONFIG[:processed_files]}/#{entry}")
    end
  end

  private

  def import_file(file, validation_only = false)
    errors = []

    dtaus = Mraba::Transaction.define_dtaus('RS', 8_888_888_888, 99_999_999, 'Credit collection')
    success_rows = []

    CSV.foreach(file, col_sep: ';', headers: true, skip_blanks: true) do |csv_row|
      import_row = ImportRow.from_row(csv_row.to_hash)
      next if import_row.activity_id.blank?
      import = import_row_with_retries(import_row, validation_only, dtaus)
      if import.failure?
        errors += import.failure
        break
      end
      success_rows << import_row.activity_id
    end

    if errors.empty? && !validation_only
      add_datei(dtaus, "#{Rails.root}/private/upload")
    end

    { success: success_rows, errors: errors }
  end

  def import_row_with_retries(row, validation_only, dtaus)
    ImportFileRow
      .new
      .with_step_args(import: [validation_only: validation_only, dtaus: dtaus])
      .call(row)
  end

  def add_datei(dtaus, source_path)
    return if dtaus.is_empty?
    FileUtils.mkdir_p "#{source_path}/csv/tmp_mraba"
    path_and_name = "#{source_path}/csv/tmp_mraba/DTAUS#{Time.now.strftime('%Y%m%d_%H%M%S')}"
    dtaus.add_datei("#{path_and_name}_201_mraba.csv")
  end
end
