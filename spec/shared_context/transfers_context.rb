shared_context 'transfers_context' do
  let(:existing_account_no) { '0000001' }
  let(:unknown_account_no) { '9999999' }
  let(:account) { double(account_no: existing_account_no) }
  let(:account_transfer) do
    double :date= => nil, :skip_mobile_tan= => nil,
           :valid? => transfer_valid?,
           errors: double(full_messages: ['validation error'])
  end
  let(:transfer_valid?) { true }
  let(:validation_only) { false }
  let(:account_transfer_data) do
    {
      'ACTIVITY_ID' => '0987',
      'DEPOT_ACTIVITY_ID' => depot_activity_id,
      'UMSATZ_KEY' => '10',
      'KONTONUMMER' => '000000001',
      'RECEIVER_BLZ' => '00000000',
      'RECEIVER_KONTO' => '000000002',
      'RECEIVER_NAME' => 'Mustermann',
      'SENDER_BLZ' => '00000000',
      'SENDER_KONTO' => sender_konto,
      'SENDER_NAME' => 'Mustermann',
      'DESC1' => 'Geld senden',
      'DESC14' => '/Last desc'
    }
  end

  let(:bank_transfer_data) do
    {
      'AMOUNT' => 10,
      'RECEIVER_NAME' => 'Bob Baumeiter',
      'RECEIVER_BLZ' => '2222222',
      'DESC1' => 'Subject',
      'SENDER_KONTO' => sender_konto,
      'RECEIVER_KONTO' => '000000002',
      'ACTIVITY_ID' => '09876',
      'SENDER_BLZ' => '00000000',
      'UMSATZ_KEY' => '10'
    }
  end
  let(:bank_transfer) do
    double valid?: bank_transfer_valid, errors: double(full_messages: ['validation error'])
  end
  let(:bank_transfer_valid) { true }

  let(:dtaus) { double }
  let(:dtaus_data) do
    {
      'ACTIVITY_ID' => '1',
      'AMOUNT' => '10',
      'RECEIVER_NAME' => 'Bob Baumeiter',
      'RECEIVER_BLZ' => '70022200',
      'DESC1' => 'Subject',
      'SENDER_KONTO' => '0101881952',
      'SENDER_BLZ' => '30020900',
      'SENDER_NAME' => 'Max Müstermänn',
      'RECEIVER_KONTO' => 'NO2',
      'UMSATZ_KEY' => '16'
    }
  end
  let(:valid_dtaus_sender?) { true }

  let(:sender_konto) { existing_account_no }
  let(:depot_activity_id) { nil }
  let(:import_row) { ImportRow.from_row(data) }

  before do
    allow(Account).to receive(:find_by_account_no).with(existing_account_no).and_return(account)
    allow(Account).to receive(:find_by_account_no).with(unknown_account_no).and_return(nil)
    account.stub_chain(:credit_account_transfers, build: account_transfer)

    allow(account).to receive(:build_transfer).and_return(bank_transfer)

    allow(dtaus).to receive(:valid_sender?)
      .with(dtaus_data['SENDER_KONTO'], dtaus_data['SENDER_BLZ'])
      .and_return(valid_dtaus_sender?)
  end
end
