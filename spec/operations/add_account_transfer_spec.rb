require 'spec_helper'

describe AddAccountTransfer do
  include_context 'transfers_context'

  subject do
    described_class
      .new.with_step_args(persist: [validation_only: validation_only])
      .call(ImportRow.from_row(data))
  end
  let(:data) { account_transfer_data }

  describe '#call' do
    context 'when invalid sender' do
      let(:sender_konto) { unknown_account_no }
      it 'fails with correct error message' do
        expect(subject).to be_failure
        expect(subject.failure).to eq('0987: Account 9999999 not found')
      end
    end

    context 'when there is no depot_activity_id' do
      let(:depot_activity_id) { nil }

      it 'creates new transfer' do
        expect(account_transfer).to receive(:save!)
        expect(subject).to be_success
        expect(subject.value).to eq(account_transfer)
      end

      context 'and validation_only = true' do
        let(:validation_only) { true }
        it 'does not save' do
          expect(account_transfer).to_not receive(:save!)
          expect(subject).to be_success
        end
      end

      context 'and transfer is invalid' do
        let(:transfer_valid?) { false }

        it 'fails with validation error' do
          expect(subject).to be_failure
          expect(subject.failure).to eq('0987: AccountTransfer validation error(s): validation error')
        end
      end
    end

    context 'depot_activity_id presents' do
      let(:wrong_depot_activity_id) { '-1' }
      let(:depot_activity_id) { '123456' }
      let(:account_state) { 'pending' }
      before do
        transfers = double('account_transfer')
        account_transfer.stub(id: 1, state: account_state, :subject= => nil)
        allow(transfers).to receive(:find_by_id).with(depot_activity_id).and_return(account_transfer)
        allow(transfers).to receive(:find_by_id).with(wrong_depot_activity_id).and_return(nil)
        allow(account).to receive(:credit_account_transfers).and_return(transfers)
      end

      it 'completes transfer' do
        expect(account_transfer).to receive(:complete_transfer!)
        expect(subject).to be_success
      end

      context 'and not pending' do
        let(:account_state) { 'deleted' }

        it 'fails with invalid state' do
          expect(subject).to be_failure
          expect(subject.failure).to eq("0987: AccountTransfer state expected 'pending' but was 'deleted'")
        end
      end

      context 'and wrong depot id' do
        let(:depot_activity_id) { wrong_depot_activity_id }

        it 'fails' do
          expect(subject).to be_failure
          expect(subject.failure).to eq('0987: AccountTransfer not found')
        end
      end

      context 'and validation_only = true' do
        let(:validation_only) { true }
        it 'does not complete' do
          expect(account_transfer).to_not receive(:complete_transfer!)
          expect(subject).to be_success
        end
      end

      context 'and transfer is invalid' do
        let(:transfer_valid?) { false }

        it 'fails with validation error' do
          expect(subject).to be_failure
          expect(subject.failure).to eq('0987: AccountTransfer validation error(s): validation error')
        end
      end
    end
  end
end
