require 'spec_helper'

describe ImportFileRow do
  include_context 'transfers_context'

  subject { described_class.new.with_step_args(import: args).call(import_row) }
  let(:validation_only) { true }
  let(:args) { [validation_only: validation_only, dtaus: dtaus] }

  describe '#call' do
    context 'when account transfer' do
      let(:data) { account_transfer_data }

      it 'returns created transfer' do
        expect(subject).to be_success
        expect(subject.value).to eq(account_transfer)
      end

      context 'and invalid transfer' do
        let(:transfer_valid?) { false }

        it 'fails with message' do
          expect(account_transfer).to_not receive(:save!)
          expect(subject).to be_failure
          expect(subject.failure.first).to include('validation error')
        end
      end
    end

    context 'when bank transfer' do
      let(:data) { bank_transfer_data }

      it 'returns created transfer' do
        expect(subject).to be_success
        expect(subject.value).to eq(bank_transfer)
      end

      context 'and invalid transfer' do
        let(:bank_transfer_valid) { false }

        it 'fails with message' do
          expect(bank_transfer).to_not receive(:save!)
          expect(subject).to be_failure
          expect(subject.failure.first).to include('validation error')
        end
      end
    end

    context 'when dtaus buchung' do
      let(:data) { dtaus_data }

      it 'success' do
        expect(dtaus).to receive(:add_buchung)
        expect(subject).to be_success
      end

      context 'and invalid sender' do
        let(:valid_dtaus_sender?) { false }

        it 'fails with message' do
          expect(dtaus).to_not receive(:add_buchung)
          expect(subject).to be_failure
          expect(subject.failure.first).to include('BLZ/Konto not valid')
        end
      end
    end

    context 'when unknown transaction type' do
      let(:data) { { 'ACTIVITY_ID' => '999', 'UMSATZ_KEY' => '16' } }

      it 'fails with message' do
        expect(subject).to be_failure
        expect(subject.failure.first).to eq('999: Transaction type not found')
      end
    end

    context 'when invalid import row' do
      let(:data) { { 'ACTIVITY_ID' => '999', 'UMSATZ_KEY' => '-1' } }

      it 'fails with validation error' do
        expect(subject).to be_failure
        expect(subject.failure.first).to eq('999: UMSATZ_KEY -1 is not allowed')
      end
    end

    context 'when exception raised' do
      let(:data) { dtaus_data }
      let(:max_retry_count) { 3 }

      before do
        args.first[:max_retry_count] = max_retry_count
        expect(dtaus).to receive(:add_buchung).exactly(max_retry_count).times.and_raise(Timeout::Error)
      end

      it 'retries max_retry_count times' do
        expect(subject).to be_failure
        expect(subject.failure.first).to eq('1: Timeout::Error')
      end
    end
  end
end
