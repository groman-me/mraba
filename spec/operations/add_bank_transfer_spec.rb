require 'spec_helper'

describe AddBankTransfer do
  include_context 'transfers_context'

  subject do
    described_class
      .new.with_step_args(persist: [validation_only: validation_only])
      .call(ImportRow.from_row(data))
  end
  let(:data) { account_transfer_data }

  describe '#call' do
    it 'creates new bank transfer' do
      expect(bank_transfer).to receive(:save!)
      expect(subject).to be_success
      expect(subject.value).to eq(bank_transfer)
    end

    context 'and validation_only = true' do
      let(:validation_only) { true }
      it 'does not save' do
        expect(bank_transfer).to_not receive(:save!)
        expect(subject).to be_success
      end
    end

    context 'when invalid sender' do
      let(:sender_konto) { unknown_account_no }
      it 'fails with correct error message' do
        expect(subject).to be_failure
        expect(subject.failure).to eq('0987: Account 9999999 not found')
      end
    end

    context 'and transfer is invalid' do
      let(:bank_transfer_valid) { false }

      it 'fails with validation error' do
        expect(subject).to be_failure
        expect(subject.failure).to eq('0987: BankTransfer validation error(s): validation error')
      end
    end
  end
end
