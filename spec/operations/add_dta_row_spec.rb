require 'spec_helper'

describe AddDtaRow do
  include_context 'transfers_context'

  subject do
    described_class.new.call(import_row: import_row, dtaus: dtaus)
  end
  let(:data) { dtaus_data }

  describe '#call' do
    it 'adds dtaus buchung' do
      expect(dtaus).to receive(:add_buchung)
      expect(subject).to be_success
    end

    context 'when invalid sender' do
      let(:valid_dtaus_sender?) { false }

      it 'fails' do
        expect(subject).to be_failure
        expect(subject.failure).to eq(['1: BLZ/Konto not valid, csv file not written'])
      end
    end
  end
end
