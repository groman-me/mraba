# encoding: utf-8
require 'spec_helper'
require_relative '../lib/csv_exporter'

describe CsvExporter do
  let(:download_folder) { "#{Rails.root}/private/data/download" }
  let(:fixtures) { "#{Rails.root}/spec/fixtures/" }
  let(:csv_file) { "#{fixtures}csv_exporter.csv" }
  let(:downloaded_mraba) { "#{download_folder}/mraba.csv" }
  let(:upload_mraba) { "#{Rails.root}/private/data/upload/mraba.csv" }
  let(:csv_exporter) { CsvExporter.new }

  describe '.transfer_and_import(send_email = true)' do
    let(:import_error) do
      "Import of the file mraba.csv failed with errors:\nImported:  Errors: 01: UMSATZ_KEY 06 is not allowed"
    end
    before(:all) { FileUtils.mkdir_p CsvExporter::CONFIG[:download_dir] }
    after(:all) { FileUtils.rm_r "#{Rails.root}/private" }

    before(:each) do
      entries = %w(mraba.csv mraba.csv.start blubb.csv)
      sftp_mock = double('sftp')
      Net::SFTP.stub(:start).and_yield(sftp_mock)
      sftp_mock.stub_chain(:dir, :entries, :map).and_return(entries)
      sftp_mock.stub(:download!).with('/data/files/csv/mraba.csv', downloaded_mraba) do
        FileUtils.cp csv_file, downloaded_mraba
      end
      sftp_mock.stub(:remove!).with('/data/files/csv/mraba.csv.start')
      sftp_mock
        .stub(:upload!)
        .with("#{Rails.root}/private/data/upload/mraba.csv", '/data/files/batch_processed/mraba.csv').once
    end

    it 'fails transfers and imports mraba csv' do
      CsvExporter.any_instance.should_receive(:upload_error_file).once.and_call_original
      BackendMailer.should_receive(:send_import_feedback).with('Import CSV failed', import_error)

      CsvExporter.transfer_and_import
      expect(File.read(upload_mraba)).to include('Import of the file mraba.csv failed with errors:')
    end

    it 'transfers and imports mraba csv' do
      expect(csv_exporter).to receive(:import).and_return('Success')
      expect(CsvExporter).to receive(:new).and_return(csv_exporter)

      expect(BackendMailer).to receive(:send_import_feedback)
        .with('Successful Import', 'Import of the file mraba.csv done.')
      CsvExporter.transfer_and_import
    end
  end
end
