# encoding: utf-8
require 'spec_helper'
require 'data_objects/import_row'

describe ImportRow do
  let(:data) do
    {
      'DEPOT_ACTIVITY_ID' => '',
      'UMSATZ_KEY' => '10',
      'KONTONUMMER' => '000000001',
      'RECEIVER_BLZ' => '00000000',
      'RECEIVER_KONTO' => '000000002',
      'RECEIVER_NAME' => 'Mustermann',
      'SENDER_BLZ' => '00000000',
      'SENDER_KONTO' => '000000003',
      'SENDER_NAME' => 'Mustermann',
      'DESC1' => 'Geld senden',
      'DESC14' => '/Last desc'
    }
  end
  let(:import_row) { ImportRow.from_row(data) }

  describe '.from_row' do
    it 'instantiate new object' do
      expect { import_row }.to_not raise_exception
      data.each_pair do |key, value|
        expect(import_row.send(key.to_sym.downcase)).to eq(value)
      end
    end

    it 'converts dates' do
      data['ENTRY_DATE'] = Time.now.strftime('%Y%m%d')
      expect(import_row.entry_date).to eq(Date.current)
    end

    it 'converts amount' do
      data['AMOUNT'] = '5'
      expect(import_row.amount).to eq(5.0)
    end

    it 'parses only allowed keys' do
      data['unknown'] = 'value'
      expect(import_row).to_not respond_to(:unknown)
    end
  end

  describe '#import_subject' do
    it 'concats description columns' do
      expect(import_row.import_subject).to eq('Geld senden/Last desc')
    end
  end

  describe '#sender' do
    let(:account) { 'account' }
    it 'cashes and returns account for sender konto' do
      expect(Account).to receive(:find_by_account_no).once.with(import_row.sender_konto).and_return(account)
      2.times { expect(import_row.sender).to eq(account) }
    end

    context 'when account does not exists' do
      let(:account) { nil }

      it 'caches empty value' do
        expect(Account).to receive(:find_by_account_no).once.with(import_row.sender_konto).and_return(account)
        2.times { expect(import_row.sender).to eq(account) }
      end
    end
  end
end
