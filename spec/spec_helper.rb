require 'bundler/setup'
require 'active_support/all'
require 'app'
require 'mocks'
require 'shared_context/transfers_context'

module Rails
  module_function

  def env
    'test'
  end

  def root
    Dir.pwd
  end

  def logger
    @logger ||= Class.new do
      def info(*args)
      end
    end.new
  end
end

RSpec.configure do |config|
end
